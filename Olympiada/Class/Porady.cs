﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Olympiada.Class
{
    public class Porady
    {
        public Dictionary<string, string> kanaly { get; private set; }

        public Porady()
        {
            kanaly = new Dictionary<string, string>();
            kanaly.Add("ct2", "Kanál ČT2");
            kanaly.Add("sport", "Kanál ČT4");
            kanaly.Add("zoh1", "Olympijský kanál 1");
            kanaly.Add("zoh2", "Olympijský kanál 2");
            kanaly.Add("zoh3", "Olympijský kanál 3");
            kanaly.Add("zoh4", "Olympijský kanál 4");
        }
    }
}
