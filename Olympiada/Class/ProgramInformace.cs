﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Olympiada.Class
{
    public class ProgramInformace
    {
        /// <summary>
        /// Unikátní ID záznamu
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Kanál na kterém je pořad vysílán
        /// </summary>
        public string stanice { get; set; }

        /// <summary>
        /// Čas začátku vysílání
        /// </summary>
        public DateTime cas_zacatek { get; set; }

        /// <summary>
        /// Čas konce vysílání pořadu
        /// </summary>
        public DateTime cas_konec { get; set; }

        /// <summary>
        /// Název sportu nebo přenosu
        /// </summary>
        public string sport { get; set; }

        /// <summary>
        /// Detailní informace o pořadu
        /// </summary>
        public string detail { get; set; }

        /// <summary>
        /// Proměnná určující ID kanálu pro listbox
        /// </summary>
        public int kanal { get; set; }
    }
}
