﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;
using System.Xml;

namespace Olympiada.Class
{
    public class XMLParse
    {
        private XmlDocument xml;

        public List<ProgramInformace> _program { get; private set; }

        public XMLParse()
        {
            xml = new XmlDocument();
            this.loadXml();
            this.parseXml();
        }

        private void parseXml()
        {
            _program = new List<ProgramInformace>();

            XmlNodeList nodes = xml.SelectNodes("/xml/stanice");
            if (nodes.Count == 0)
                throw new ArgumentOutOfRangeException("Stanice nejsou dostupné");

            foreach (XmlNode item in nodes)
            {
                string name = item.Attributes["id"].InnerText;

                XmlNodeList porady = item.SelectNodes("program");
                foreach (XmlNode porad in porady)
                {
                    ProgramInformace tmp = new ProgramInformace();
                    tmp.stanice = name;

                    string cas_zac = porad.SelectSingleNode("cas_zac").InnerText;
                    string cas_kon = porad.SelectSingleNode("cas_kon").InnerText;

                    tmp.id = int.Parse(porad.SelectSingleNode("id").InnerText);
                    tmp.cas_zacatek = this.parseDate(cas_zac);
                    tmp.cas_konec = this.parseDate(cas_kon);
                    tmp.sport = porad.SelectSingleNode("sport").InnerText;
                    tmp.detail = porad.SelectSingleNode("detail").InnerText;
                    _program.Add(tmp);
                }
            }
        }

        private void loadXml()
        {
            try
            {
                WebClient wc = new WebClient();
                string tmp = wc.DownloadString(new Uri("http://download.vymakdevel.cz/vymakdevel/"));
                xml.LoadXml(tmp);
            }
            catch (WebException) 
            {
                throw new WebException();
            }
        }

        private DateTime parseDate(string date)
        {
            string[] tmp = date.Split(':');

            DateTime now = DateTime.Now;
            DateTime dateTime = new DateTime(now.Year, now.Month, now.Day, int.Parse(tmp[0]), int.Parse(tmp[1]), 0);

            return dateTime;
        }
    }
}
