﻿using Olympiada.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Olympiada
{
    public partial class ProgramHer : Form
    {
        private OlympiadaForm olympiada;

        private XMLParse parser;

        private List<ProgramInformace> _aktualniStanice;

        private ProgramInformace currentProgram;

        public ProgramHer(OlympiadaForm olympiada)
        {
            InitializeComponent();
            this.olympiada = olympiada;
            parser = new XMLParse();

            int index = this.olympiada.programListBox.SelectedIndex;
            comboBoxSelectStation.SelectedIndex = (index != -1) ? index : 0;
            timerUpdate.Start();
            timerTime.Start();
            actualTimeLabel.Text = DateTime.Now.ToLongTimeString();
            this.TopMost = (this.olympiada.alwaysOnTopCheck.Checked) ? true : false;
        }

        private void comboBoxSelectStation_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = comboBoxSelectStation.SelectedItem.ToString();

            _aktualniStanice = new List<ProgramInformace>();
            bool isFirtsEntryModified = false;
            foreach (ProgramInformace item in parser._program)
            {
                if (item.stanice == name)
                {
                    if (!isFirtsEntryModified)
                    {
                        item.cas_zacatek = item.cas_zacatek.AddDays(-1);
                        isFirtsEntryModified = true;
                    }

                    item.kanal = comboBoxSelectStation.SelectedIndex;
                    _aktualniStanice.Add(item);
                }
            }
            this.updateGridView();
        }

        private void updateGridView()
        {
            poradProgressBar.Visible = false;
            currentProgram = null;
            dataGridView1.Rows.Clear();
            foreach (ProgramInformace item in _aktualniStanice)
            {
                DateTime now = DateTime.Now;
                bool current = (item.cas_zacatek < now && item.cas_konec > now) ? true : false;
                string text = (this.olympiada._notifier.ContainsKey(item.id)) ? "Zrušit" : "Upozornit";

                if (item.cas_zacatek < DateTime.Now)
                    text = String.Empty;

                dataGridView1.Rows.Add(item.sport, item.detail, item.cas_zacatek.ToLongTimeString() + " - " + item.cas_konec.ToLongTimeString(), text, current, item.id);
            }

            label3.Visible = false;
            currentSportLabel.Visible = false;
            detailRichTextBox.Visible = false;
            foreach (DataGridViewRow item in dataGridView1.Rows)
            {
                bool isCurrent = (bool)item.Cells[4].Value;
                string text = (string)item.Cells[3].Value;

                if (isCurrent)
                {
                    poradProgressBar.Visible = true;
                    dataGridView1.FirstDisplayedScrollingRowIndex = item.Index;
                    item.DefaultCellStyle.BackColor = System.Drawing.Color.LightGreen;
                    item.Cells[3].Value = "Sledovat";

                    currentProgram = _aktualniStanice[item.Index];
                    currentSportLabel.Text = currentProgram.sport;
                    detailRichTextBox.Text = currentProgram.detail;
                    label3.Visible = true;
                    currentSportLabel.Visible = true;
                    detailRichTextBox.Visible = true;
                }
                else if (text == "Zrušit")
                {
                    item.DefaultCellStyle.BackColor = System.Drawing.Color.LightYellow;
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                int rowIndex = e.RowIndex;
                string cellText = (string)dataGridView1[3, e.RowIndex].Value;
                ProgramInformace info = _aktualniStanice[rowIndex];

                switch (cellText)
                {
                    case "Upozornit":
                        if (!this.olympiada._notifier.ContainsKey(info.id))
                            this.olympiada._notifier.Add(info.id, info);
                        this.updateGridView();
                        break;
                    case "Zrušit":
                        this.olympiada._notifier.Remove(info.id);
                        this.updateGridView();
                        break;
                    case "Sledovat":
                        this.olympiada.programListBox.SelectedIndex = comboBoxSelectStation.SelectedIndex;
                        break;
                }
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            this.updateGridView();
        }

        private void autoUpdateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (autoUpdateCheckBox.Checked)
                timerUpdate.Start();
            else
                timerUpdate.Stop();
        }

        private void timerTime_Tick(object sender, EventArgs e)
        {
            actualTimeLabel.Text = DateTime.Now.ToLongTimeString();

            if (currentProgram != null)
            {
                TimeSpan totalMinute = currentProgram.cas_konec - currentProgram.cas_zacatek;
                TimeSpan lapsedMinute = DateTime.Now - currentProgram.cas_zacatek;

                double percent = ((double)lapsedMinute.TotalMinutes / (double)totalMinute.TotalMinutes) * 100;
                poradProgressBar.Value = (int)percent;
                toolTip1.SetToolTip(poradProgressBar, "Uplynulo již: " + (int)percent + "%");

                if (percent == 100)
                {
                    this.updateGridView();
                    poradProgressBar.Visible = false;
                }
            }
        }
    }
}
