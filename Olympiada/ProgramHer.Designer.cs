﻿namespace Olympiada
{
    partial class ProgramHer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProgramHer));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.sport = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.detail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.upozornit = new System.Windows.Forms.DataGridViewLinkColumn();
            this.aktualni = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBoxSelectStation = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.closeButton = new System.Windows.Forms.Button();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.autoUpdateCheckBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.actualTimeLabel = new System.Windows.Forms.Label();
            this.timerTime = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.currentSportLabel = new System.Windows.Forms.Label();
            this.detailRichTextBox = new System.Windows.Forms.RichTextBox();
            this.poradProgressBar = new System.Windows.Forms.ProgressBar();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sport,
            this.detail,
            this.cas,
            this.upozornit,
            this.aktualni,
            this.id});
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridView1.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.RowTemplate.Height = 40;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(620, 265);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // sport
            // 
            this.sport.HeaderText = "Sport";
            this.sport.Name = "sport";
            this.sport.ReadOnly = true;
            this.sport.Width = 150;
            // 
            // detail
            // 
            this.detail.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.detail.HeaderText = "Detailní informace";
            this.detail.Name = "detail";
            this.detail.ReadOnly = true;
            // 
            // cas
            // 
            this.cas.HeaderText = "Čas";
            this.cas.Name = "cas";
            this.cas.ReadOnly = true;
            this.cas.Width = 130;
            // 
            // upozornit
            // 
            this.upozornit.HeaderText = "Upozornit";
            this.upozornit.Name = "upozornit";
            this.upozornit.ReadOnly = true;
            this.upozornit.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.upozornit.Width = 60;
            // 
            // aktualni
            // 
            this.aktualni.HeaderText = "Právě běží";
            this.aktualni.Name = "aktualni";
            this.aktualni.ReadOnly = true;
            this.aktualni.Visible = false;
            // 
            // id
            // 
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // comboBoxSelectStation
            // 
            this.comboBoxSelectStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSelectStation.FormattingEnabled = true;
            this.comboBoxSelectStation.Items.AddRange(new object[] {
            "ČT2",
            "ČT sport",
            "ZOH1",
            "ZOH2",
            "ZOH3",
            "ZOH4"});
            this.comboBoxSelectStation.Location = new System.Drawing.Point(641, 28);
            this.comboBoxSelectStation.Name = "comboBoxSelectStation";
            this.comboBoxSelectStation.Size = new System.Drawing.Size(168, 21);
            this.comboBoxSelectStation.TabIndex = 1;
            this.comboBoxSelectStation.SelectedIndexChanged += new System.EventHandler(this.comboBoxSelectStation_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(638, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Vyberte prosím kanál";
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(641, 254);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(93, 23);
            this.closeButton.TabIndex = 3;
            this.closeButton.Text = "Zavřít program";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // timerUpdate
            // 
            this.timerUpdate.Interval = 60000;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // autoUpdateCheckBox
            // 
            this.autoUpdateCheckBox.AutoSize = true;
            this.autoUpdateCheckBox.Checked = true;
            this.autoUpdateCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoUpdateCheckBox.Location = new System.Drawing.Point(641, 231);
            this.autoUpdateCheckBox.Name = "autoUpdateCheckBox";
            this.autoUpdateCheckBox.Size = new System.Drawing.Size(144, 17);
            this.autoUpdateCheckBox.TabIndex = 4;
            this.autoUpdateCheckBox.Text = "Automaticky aktualizovat";
            this.autoUpdateCheckBox.UseVisualStyleBackColor = true;
            this.autoUpdateCheckBox.CheckedChanged += new System.EventHandler(this.autoUpdateCheckBox_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(638, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Aktuální čas:";
            // 
            // actualTimeLabel
            // 
            this.actualTimeLabel.AutoSize = true;
            this.actualTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.actualTimeLabel.Location = new System.Drawing.Point(636, 79);
            this.actualTimeLabel.Name = "actualTimeLabel";
            this.actualTimeLabel.Size = new System.Drawing.Size(70, 26);
            this.actualTimeLabel.TabIndex = 6;
            this.actualTimeLabel.Text = "label3";
            // 
            // timerTime
            // 
            this.timerTime.Interval = 1000;
            this.timerTime.Tick += new System.EventHandler(this.timerTime_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(638, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Právě běží:";
            // 
            // currentSportLabel
            // 
            this.currentSportLabel.AutoSize = true;
            this.currentSportLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.currentSportLabel.Location = new System.Drawing.Point(636, 135);
            this.currentSportLabel.Name = "currentSportLabel";
            this.currentSportLabel.Size = new System.Drawing.Size(57, 20);
            this.currentSportLabel.TabIndex = 8;
            this.currentSportLabel.Text = "label3";
            // 
            // detailRichTextBox
            // 
            this.detailRichTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.detailRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.detailRichTextBox.Location = new System.Drawing.Point(640, 158);
            this.detailRichTextBox.Name = "detailRichTextBox";
            this.detailRichTextBox.ReadOnly = true;
            this.detailRichTextBox.Size = new System.Drawing.Size(169, 23);
            this.detailRichTextBox.TabIndex = 9;
            this.detailRichTextBox.Text = "";
            // 
            // poradProgressBar
            // 
            this.poradProgressBar.Location = new System.Drawing.Point(641, 187);
            this.poradProgressBar.Name = "poradProgressBar";
            this.poradProgressBar.Size = new System.Drawing.Size(168, 14);
            this.poradProgressBar.TabIndex = 10;
            this.poradProgressBar.Visible = false;
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // ProgramHer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 287);
            this.Controls.Add(this.poradProgressBar);
            this.Controls.Add(this.detailRichTextBox);
            this.Controls.Add(this.currentSportLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.actualTimeLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.autoUpdateCheckBox);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxSelectStation);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProgramHer";
            this.Text = "Program vysílání ČT";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBoxSelectStation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn sport;
        private System.Windows.Forms.DataGridViewTextBoxColumn detail;
        private System.Windows.Forms.DataGridViewTextBoxColumn cas;
        private System.Windows.Forms.DataGridViewLinkColumn upozornit;
        private System.Windows.Forms.DataGridViewCheckBoxColumn aktualni;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Timer timerUpdate;
        private System.Windows.Forms.CheckBox autoUpdateCheckBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label actualTimeLabel;
        private System.Windows.Forms.Timer timerTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label currentSportLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.RichTextBox detailRichTextBox;
        private System.Windows.Forms.ProgressBar poradProgressBar;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}