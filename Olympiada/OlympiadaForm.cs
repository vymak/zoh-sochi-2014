﻿using Olympiada.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Olympiada
{
    public partial class OlympiadaForm : Form
    {
        private Porady por;
        private string[] programLoc;

        private ProgramHer _programHer;

        /// <summary>
        /// Proměnná udržující události na které má být upozorněno
        /// </summary>
        public Dictionary<int, ProgramInformace> _notifier;

        public OlympiadaForm()
        {
            InitializeComponent();
            
            por = new Porady();
            _notifier = new Dictionary<int, ProgramInformace>();
            programLoc = new string[por.kanaly.Count];
            
            naplnListbox();
            kontextMenu();
            checkDate();
            
            programListBox.SelectedIndexChanged += listBox1_SelectedIndexChanged;
            webBrowser1.DocumentCompleted += webBrowser1_DocumentCompleted;

            webBrowser1.ScriptErrorsSuppressed = true;
            pictureBox1.Visible = false;
            label2.Visible = false;
            webBrowser1.ScrollBarsEnabled = false;
            label4.Visible = false;
            panel1.Visible = false;

            this.Size = new System.Drawing.Size(657, 391);

            notifierTimer.Interval = 600;
            notifierTimer.Start();

            toolTip1.SetToolTip(autochangeCheckBox, "Automaticky přepnout kanál při vyvolání upozornění na zvolený pořad");
            toolTip1.SetToolTip(alwaysOnTopCheck, "Určuje, zda-li se program bude vždy umístěn nad ostatními okny");
        }

        private void checkDate()
        {
            DateTime olympicEnd = new DateTime(2014, 02, 23, 23, 59, 59);

            if (DateTime.Now > olympicEnd)
            {
                MessageBox.Show("Program již není možné spouštět, protože olympíjské hry již skončili!", "Chyba při spuštění", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webBrowser1.Document.Body.Style = "overflow:hidden";
            webBrowser1.Document.Window.ScrollTo(25, 266);
            webBrowser1.Visible = true;
        }

        private void kontextMenu()
        {
            ContextMenu cm = new ContextMenu();
            cm.MenuItems.Add("Nastavení", nastaveni);
            this.ContextMenu = cm;
        }

        private void nastaveni(object sender, EventArgs e)
        {
            if (panel1.Visible)
            {
                this.Size = new System.Drawing.Size(646, 391);
                panel1.Visible = false;
                label4.Visible = true;
                label4.Location = new System.Drawing.Point(579, 0);
                label4.Text = "Nastavení";
                label4.Font = new Font(label4.Font, FontStyle.Regular);
            }
            else
            {
                pictureBox3.Visible = false;
                this.Size = new System.Drawing.Size(780, 391);
                panel1.Visible = true;
                label4.Location = new System.Drawing.Point(641, 280);
                label4.Text = "Skrýt nastavení";
                label4.Visible = true;
                label4.Font = new Font(label4.Font, FontStyle.Bold);
            }
        }

        void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string curItem = programListBox.SelectedItem.ToString();
            int index = programListBox.FindString(curItem);
            webBrowser1.Visible = false;
            panel2.Visible = false;

            try
            {
                webBrowser1.Url = new Uri("http://www.ceskatelevize.cz/zoh/zive/" + programLoc[index] + "/");
                pictureBox1.Visible = true;
                label2.Visible = true;
                pictureBox1.Image = (Image)Properties.Resources.ResourceManager.GetObject(programLoc[index]);
            }
            catch (Exception)
            {
                MessageBox.Show("Při spouštění streamu došlo k chybě", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void naplnListbox()
        {
            int i = 0;
            foreach (var item in por.kanaly)
            {
                programListBox.Items.Add(item.Value);
                programLoc[i++] = item.Key;
            }
        }

        private void alwaysOnTopCheck_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = (alwaysOnTopCheck.Checked) ? true : false;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://git.vymak.cz");
            }
            catch (Exception)
            {
                MessageBox.Show("Při spouštění webového prohlížeče došlo k chybě", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void programLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                _programHer = new ProgramHer(this);
                _programHer.Show();
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("Při stahování programu došlo k chybě! Program pravděpodobně není dostupný!", "Chyba při stahování programu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (WebException)
            {
                DialogResult res = MessageBox.Show("Při stahování programu došlo k chybě! Zkontrolujte prosím Vaše internetové připojení a zkuste to znovu.\n\nPřejete si zkusit stáhnout program znovu?", "Chyba pči stahování dat", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                
                if (res == System.Windows.Forms.DialogResult.Yes)
                    this.programLinkLabel_LinkClicked(sender, e);
            }
        }

        private void notifierTimer_Tick(object sender, EventArgs e)
        {
            if (_notifier.Count != 0)
            {
                foreach (KeyValuePair<int, ProgramInformace> item in _notifier)
                {
                    if (item.Value.cas_zacatek.Hour == DateTime.Now.Hour && item.Value.cas_zacatek.Minute == DateTime.Now.Minute)
                    {
                        notifierTimer.Stop();
                        this.TopMost = false;
                        if (autochangeCheckBox.Checked)
                            programListBox.SelectedIndex = item.Value.kanal;
                        else
                        {
                            DialogResult res = MessageBox.Show("Program, na který chcete být upozorněn právě začíná!\n\nSport: " + item.Value.sport + "\n" + "Detail: " + item.Value.detail + "\n\nPřejete si program přepnout?", "Upozornění na pořad", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (res == System.Windows.Forms.DialogResult.Yes)
                                programListBox.SelectedIndex = item.Value.kanal;
                        }
                        _notifier.Remove(item.Key);
                        notifierTimer.Start();
                        this.TopMost = alwaysOnTopCheck.Checked;
                        break;
                    }
                }
            }
        }
    }
}
