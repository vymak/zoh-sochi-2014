﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Olympiáda Sochi 2014")]
[assembly: AssemblyDescription("Sledování všech kanálů, které nabízí ČT na svých stránkách. Dostupnost a kvalita streamu je závislá na poskytovaném streamu ze strany ČT")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vymakdevel.cz")]
[assembly: AssemblyProduct("Olympiada Sochi 2014")]
[assembly: AssemblyCopyright("Copyright © Vymakdevel.cz 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a7c532c5-6d1a-4b72-8940-92c337952863")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2.3.1211")]
[assembly: AssemblyFileVersion("1.2.3.1211")]
