<?php

require 'simple_html_dom.php';

$html = file_get_html('http://www.ceskatelevize.cz/zoh/zive/');
$element = $html->find('div[class=zohProgrammeProgramPanel] div[class=channel]');

$xml = new SimpleXMLElement('<xml/>');
$i = 0;
foreach ($element as $value) {
    $title = trim($value->find('div[class=channelTitle]', 0)->plaintext);

    $track = $xml->addChild('stanice');
    $track->addAttribute('id', $title);

    foreach ($value->find('div[class=broadcast]') as $item) {
        $staniceItem = $track->addChild('program');


        $time = trim($item->find('p', 0)->plaintext);
        $time_zac = substr($time, 0, 5);
        $time_kon = substr($time, 8, 5);

        $name = trim($item->find('span', 0)->plaintext);
        $detail = trim($item->find('span', 1)->plaintext);

        $staniceItem->addChild('id', $i++);
        $staniceItem->addChild('cas_zac', $time_zac);
        $staniceItem->addChild('cas_kon', $time_kon);
        $staniceItem->addChild('sport', $name);
        $staniceItem->addChild('detail', $detail);
    }
}
Header('Content-type: text/xml');
print($xml->asXML());
